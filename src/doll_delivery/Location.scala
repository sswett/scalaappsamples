package doll_delivery

import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayBuffer


class Location(nameIn: String) {

  val name = nameIn
  private val neighborsWithEdgeReferences = HashMap[Location,Edge]() 
  private var tentativeDistance = Integer.MAX_VALUE
  private var visited = false
  private var parent: Location = null
  
  
  def getTentativeDistance() : Integer = 
  {
    return tentativeDistance;
  }
  
  
  def isVisited() : Boolean =
  {
    return visited;
  }
    
  
  def addNeighbor(neighbor : Location, edge : Edge)
  {
    neighborsWithEdgeReferences.put(neighbor, edge)
  }
  
  
  def setTentativeDistance(tentativeDistance : Integer)
  {
    this.tentativeDistance = tentativeDistance;
  }

  
  def markAsVisited()
  {
    visited = true;
  }
  
  
  def getUnvisitedNeighbors() : ArrayBuffer[Location] =
  {
    var result = ArrayBuffer[Location]()
    
    for ((neighbor, location) <- neighborsWithEdgeReferences)
    {
      if (!neighbor.isVisited())
      {
        result += neighbor
      }
    }
    
    return result
  }  
    
  
  def getDistanceToNeighbor(neighbor : Location) : Integer =
  {
    var edge = neighborsWithEdgeReferences.get(neighbor).getOrElse(null);
    
    if (edge == null)
    {
      // TODO - this shouldn't happen; perhaps throw exception
      // _errorLogger.error("Location.getDistancetoNeighbor -- got unexpected 0 result.");
      println("Location.getDistancetoNeighbor -- got unexpected 0 result.")
      
      return 0   // Apparently this might be reached.  It's important to return 0 here.
    }
    else
    {
      return edge.distanceBetween
    }
  }
    
  
  def setParent(parent : Location)
  {
    this.parent = parent;
  }
  
  
  def getParent() : Location =
  {
    parent
  }

  
}
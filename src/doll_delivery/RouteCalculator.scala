package doll_delivery

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap


object RouteCalculator {

	val START_LOCATION_KEY  = "startLocation"
	val END_LOCATION_KEY  = "endLocation"
	val DISTANCE_KEY  = "distance"
	val PATH_KEY  = "path"
  
  val LOCATION_KEYS = Set(START_LOCATION_KEY, END_LOCATION_KEY)
	val PATH_ELEMENT_SEPARATOR = " => "

	var calculationComplete = false


	def getGreeting(name: String) : String = 
  {
			"Hello, " + name    
	}


	def getDistanceAndPathToDestination(
			startingLocation : String, 
			targetLocation : String, 
			edges : List[Map[String, Any]]) : Map[String,Any]  =	
  {

    calculationComplete = false
    
    // Convert generally-typed input arguments into more useful Location objects with Edge references:    

			// Goal: create a unique list of Location objects:

			val locations = ArrayBuffer[Location]()

			//    First build a unique map of locations.  Key = location name, Value = Location object

			val nameLocationMap = HashMap[String,Location]()
      
			for (edgeMap <- edges)
			{
        LOCATION_KEYS.foreach 
        {
          locationKey =>
          val locationName = getLocatonNameFromEdgeMap(edgeMap, locationKey)

          if (!nameLocationMap.contains(locationName))
          {
            val location = new Location(locationName) 
            nameLocationMap.put(locationName, location)
        
            // Also add to master list of Location objects
            locations += location
          }
        }
      }

			// Unique list of Location objects is now built, but make note of neighbor associations through use of Edge objects:

			for (edgeMap <- edges)
			{
        val startLocationName = getLocatonNameFromEdgeMap(edgeMap, START_LOCATION_KEY)
        val endLocationName = getLocatonNameFromEdgeMap(edgeMap, END_LOCATION_KEY)
        val distance = getDistanceFromEdgeMap(edgeMap)

        val startLocation = getLocatonFromNameLocationMap(nameLocationMap, startLocationName)   
        val endLocation = getLocatonFromNameLocationMap(nameLocationMap, endLocationName)

				val edge = new Edge(startLocation, endLocation, distance)
				startLocation.addNeighbor(endLocation, edge)
				endLocation.addNeighbor(startLocation, edge)
			}
      
      // Now that generally-typed input arguments have been converted to Location objects (with neighbor associations), 
      // calculate distance and path between two Location objects:

      val startLocation = getLocatonFromNameLocationMap(nameLocationMap, startingLocation)
      val endLocation = getLocatonFromNameLocationMap(nameLocationMap, targetLocation)
      
      try 
      {
        calculateDistanceAndPath(startLocation, endLocation, locations)
      } 
      catch 
      {
         case ex: NullPointerException =>
           {
            println("Exception occurred.  Returning empty result")
            ex.printStackTrace()
            getOutputMapAfterCalculations(startLocation, endLocation)   // Will return empty result since calculationComplete is false 
           }
      }
      
	}
  
  
  private def getLocatonNameFromEdgeMap(edgeMap: Map[String, Any], locationKey: String) : String =
  {
    edgeMap.get(locationKey).getOrElse(null).asInstanceOf[String]    
  }
  
  
  private def getDistanceFromEdgeMap(edgeMap: Map[String, Any]) : Integer =
  {
    edgeMap.get(DISTANCE_KEY).getOrElse(null).asInstanceOf[Integer]    
  }
  
  
  private def getLocatonFromNameLocationMap(nameLocationMap: HashMap[String, Location], locationName: String) : Location =
  {
    nameLocationMap.get(locationName).getOrElse(null)
  }
  

  @throws(classOf[NullPointerException])
	private def calculateDistanceAndPath(
			startLocation : Location, 
			endLocation : Location, 
			locations : ArrayBuffer[Location]) : Map[String,Any]  =
		{

			println("Begin calculations for ... start location: " + startLocation.name + ", end location: " + endLocation.name )
      
      
      // The logic below generally follows the "narrative algorithm" of Dijkstra's at http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm


			// Set start Location tentative distance to zero
			startLocation.setTentativeDistance(0)

			// All other locations' tentative distances were set to "infinity" during Location object instantiation
      // All other locations' "visited" flags were set to false during Location object instantiation

			// Set the "current location" to be the start location
			var currentLocation = startLocation
			currentLocation.markAsVisited()

			// Create a "set" of all unvisited locations
			val unvisitedLocations = locations.-(currentLocation)

			var finished = false

					while (!finished)
					{
						// Consider each of the current location's unvisited neighbors and calculate the tentative cumulative distances to those neighbors, storing
            // the results in the neighbor's Location instance

						for (unvisitedNeighbor <- currentLocation.getUnvisitedNeighbors())
						{
							// Proposed cumulative distance to the unvisited neighbor is the current location's distance plus distance to the neighbor
							val proposedDistance = currentLocation.getTentativeDistance() + currentLocation.getDistanceToNeighbor(unvisitedNeighbor)

              // If the proposed distance is lower than what was previously stored in the unvisitedNeighbor instance, then save it into the instance
							if (proposedDistance < unvisitedNeighbor.getTentativeDistance())
							{
								unvisitedNeighbor.setTentativeDistance(proposedDistance)
								unvisitedNeighbor.setParent(currentLocation)
							}
						}

						// Mark the current location as visited and remove it from the overall unvisited "set"
						currentLocation.markAsVisited()
						unvisitedLocations -= currentLocation

								// If we have reached the end location, processing is finished
								if (currentLocation.equals(endLocation))
								{
									println("Found path to end location.")
									finished = true
								}

								else

								{
									// Calculate bestNextLocation.  Consider all nodes with computed distances (not just neighbors)
									val bestNextLocation = getUnvisitedLocationWithSmallestTentativeDistance(unvisitedLocations);

									// If the smallest tentative distance to an unvisited location is "infinity", processing is finished
									if (bestNextLocation == null)
									{
										println("bestNextLocation is null.  endLocation parent is: " + endLocation.getParent() + 
												"; all of the unvisited locations have a tentative distance of: " + Integer.MAX_VALUE)

										finished = true
									}

                  // Otherwise, stay in the "while" loop and process the bestNextLocation
									else
									{
										currentLocation = bestNextLocation
									}

								}

					}

			calculationComplete = true

			getOutputMapAfterCalculations(startLocation, endLocation)
		}


	private def getUnvisitedLocationWithSmallestTentativeDistance(unvisitedLocations : ArrayBuffer[Location]) : Location =
		{
			var smallest = Integer.MAX_VALUE
			var bestLocation : Location = null

			for (location <- unvisitedLocations)
			{
				if (location.getTentativeDistance() < smallest)
				{
					smallest = location.getTentativeDistance()
					bestLocation = location
				}
			}

			bestLocation
		}


	private def getOutputMapAfterCalculations(startLocation: Location, endLocation: Location) : Map[String,Any] =
		{
			if (!calculationComplete || endLocation.getParent() == null)
			{
				val outputMap = Map[String,Any]()
			  return outputMap
			}

			// Path reconstruction:

			val path = new StringBuilder()
			var i = endLocation

			while (!i.equals(startLocation))
			{
				path.insert(0, PATH_ELEMENT_SEPARATOR + i.name)
				i = i.getParent()
			}

			path.insert(0,  startLocation.name)

			Map( DISTANCE_KEY -> endLocation.getTentativeDistance(), PATH_KEY -> path.toString() )
		}


}
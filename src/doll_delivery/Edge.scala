package doll_delivery

class Edge(location1In: Location, location2In: Location, distanceBetweenIn: Integer) {

  val location1: Location = location1In
  val location2: Location = location2In
  val distanceBetween: Integer = distanceBetweenIn
    
}
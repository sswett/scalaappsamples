package tests

import doll_delivery._
import scala.collection.mutable.ArrayBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before


class DollDeliveryTest {
  
      val goodEdgeList = List(
        Map("startLocation" -> "Kruthika's abode", "endLocation" -> "Mark's crib", "distance" -> 9),
        Map("startLocation" -> "Kruthika's abode", "endLocation" -> "Greg's casa", "distance" -> 4),
        Map("startLocation" -> "Kruthika's abode", "endLocation" -> "Matt's pad", "distance" -> 18),
        Map("startLocation" -> "Kruthika's abode", "endLocation" -> "Brian's apartment", "distance" -> 8),
        Map("startLocation" -> "Brian's apartment", "endLocation" -> "Wesley's condo", "distance" -> 7),
        Map("startLocation" -> "Brian's apartment", "endLocation" -> "Cam's dwelling", "distance" -> 17),
        Map("startLocation" -> "Greg's casa", "endLocation" -> "Cam's dwelling", "distance" -> 13),
        Map("startLocation" -> "Greg's casa", "endLocation" -> "Mike's digs", "distance" -> 19),
        Map("startLocation" -> "Greg's casa", "endLocation" -> "Matt's pad", "distance" -> 14),
        Map("startLocation" -> "Wesley's condo", "endLocation" -> "Kirk's farm", "distance" -> 10),
        Map("startLocation" -> "Wesley's condo", "endLocation" -> "Nathan's flat", "distance" -> 11),
        Map("startLocation" -> "Wesley's condo", "endLocation" -> "Bryce's den", "distance" -> 6),
        Map("startLocation" -> "Matt's pad", "endLocation" -> "Mark's crib", "distance" -> 19),
        Map("startLocation" -> "Matt's pad", "endLocation" -> "Nathan's flat", "distance" -> 15),
        Map("startLocation" -> "Matt's pad", "endLocation" -> "Craig's haunt", "distance" -> 14),
        Map("startLocation" -> "Mark's crib", "endLocation" -> "Kirk's farm", "distance" -> 9),
        Map("startLocation" -> "Mark's crib", "endLocation" -> "Nathan's flat", "distance" -> 12),
        Map("startLocation" -> "Bryce's den", "endLocation" -> "Craig's haunt", "distance" -> 10),
        Map("startLocation" -> "Bryce's den", "endLocation" -> "Mike's digs", "distance" -> 9),
        Map("startLocation" -> "Mike's digs", "endLocation" -> "Cam's dwelling", "distance" -> 20),
        Map("startLocation" -> "Mike's digs", "endLocation" -> "Nathan's flat", "distance" -> 12),
        Map("startLocation" -> "Cam's dwelling", "endLocation" -> "Craig's haunt", "distance" -> 18),
        Map("startLocation" -> "Nathan's flat", "endLocation" -> "Kirk's farm", "distance" -> 3)
        )

      // Good locations:
        
      val kruth = "Kruthika's abode"
      val craig = "Craig's haunt"
      val greg = "Greg's casa"
      val wes = "Wesley's condo"
      val mark = "Mark's crib"
      val bryce = "Bryce's den"
      val mike = "Mike's digs"
      
      // Bad locations:
        
      val badStartLocation = "bad start"
      val badEndLocation = "bad end"
      

      // Note: there are intentional missing connections between locations in the edge list below
        
      val badEdgeList = List(
        Map("startLocation" -> "1", "endLocation" -> "2", "distance" -> 5),
        Map("startLocation" -> "2", "endLocation" -> "3", "distance" -> 1),
        Map("startLocation" -> "2", "endLocation" -> "4", "distance" -> 2),
        Map("startLocation" -> "3", "endLocation" -> "4", "distance" -> -8),   // negative distance (problem)
        Map("startLocation" -> "5", "endLocation" -> "5", "distance" -> 0)   // edge to self (problem)
        )

        
      @Test def testVariousDistances() {
        
        printTestMethodName()
        
        var outputMap = RouteCalculator.getDistanceAndPathToDestination(kruth, craig, goodEdgeList)
    	  assertTrue(outputMap.size == 2)
    	  assertTrue(getDistanceFromOutputMap(outputMap) == 31)
    
        outputMap = RouteCalculator.getDistanceAndPathToDestination(greg, wes, goodEdgeList)
        assertTrue(getDistanceFromOutputMap(outputMap) == 19)
    
        outputMap = RouteCalculator.getDistanceAndPathToDestination(mark, bryce, goodEdgeList)
        assertTrue(getDistanceFromOutputMap(outputMap) == 25)
        
      }
      
      
      @Test def testSameDistanceForwardBackward() {
        
        printTestMethodName()

        var outputMap = RouteCalculator.getDistanceAndPathToDestination(kruth, craig, goodEdgeList)
        var distance1 = getDistanceFromOutputMap(outputMap)
    
        outputMap = RouteCalculator.getDistanceAndPathToDestination(craig, kruth, goodEdgeList)
        var distance2 = getDistanceFromOutputMap(outputMap)
    
        assertTrue(distance1 == distance2)
        
        outputMap = RouteCalculator.getDistanceAndPathToDestination(mark, bryce, goodEdgeList)
        distance1 = getDistanceFromOutputMap(outputMap)
    
        outputMap = RouteCalculator.getDistanceAndPathToDestination(bryce, mark, goodEdgeList)
        distance2 = getDistanceFromOutputMap(outputMap)
    
        assertTrue(distance1 == distance2)

      }
      
      
      @Test def testSameLocationsForwardBackward() {
        
        printTestMethodName()

        var outputMap = RouteCalculator.getDistanceAndPathToDestination(kruth, craig, goodEdgeList)
        var locationNames1 = getPathAsArrayFromOutputMap(outputMap)

        outputMap = RouteCalculator.getDistanceAndPathToDestination(craig, kruth, goodEdgeList)
        var locationNames2 = getPathAsArrayFromOutputMap(outputMap)

        assertTrue(locationNames1.length == locationNames2.length);
        assertTrue(doReverseArraysMatch(locationNames1, locationNames2));
    
        outputMap = RouteCalculator.getDistanceAndPathToDestination(mark, mike, goodEdgeList)
        locationNames1 = getPathAsArrayFromOutputMap(outputMap)

        outputMap = RouteCalculator.getDistanceAndPathToDestination(mike, mark, goodEdgeList)
        locationNames2 = getPathAsArrayFromOutputMap(outputMap)

        assertTrue(locationNames1.length == locationNames2.length)
        assertTrue(doReverseArraysMatch(locationNames1, locationNames2))
        
      }
      
      
      @Test def testLocationsDontExistInEdgesExpectingException() {
        
        printTestMethodName()

        val outputMap = RouteCalculator.getDistanceAndPathToDestination(badStartLocation, badEndLocation, goodEdgeList)
        assertTrue(outputMap.size == 0)
      }
      
      
      @Test def testStartAndEndLocationsAreTheSame() {
        
        printTestMethodName()

        val outputMap = RouteCalculator.getDistanceAndPathToDestination("5", "5", badEdgeList)
        assertTrue(outputMap.size == 0)
      }
      
      
      @Test def testUnreachableLocations() {
        
        printTestMethodName()
        
        val outputMap = RouteCalculator.getDistanceAndPathToDestination("1", "5", badEdgeList)
        assertTrue(outputMap.size == 0)
      }
      
      
      @Test def testNegativeDistance() {
        
        printTestMethodName()
        
        val outputMap = RouteCalculator.getDistanceAndPathToDestination("3", "4", badEdgeList)
        assertTrue(outputMap.size == 2)
        val distance = getDistanceFromOutputMap(outputMap)
        assertTrue(distance == -8)
      }
      
      
      private def getDistanceFromOutputMap(map: Map[String,Any]) : Integer = {
        map.get(RouteCalculator.DISTANCE_KEY).getOrElse(null).asInstanceOf[Integer]
      }
      
      
      private def getPathAsArrayFromOutputMap(map: Map[String,Any]) : Array[String] = {
        val path = map.get(RouteCalculator.PATH_KEY).getOrElse(null).asInstanceOf[String]
        path.split(RouteCalculator.PATH_ELEMENT_SEPARATOR)
      }
      
      
      private def doReverseArraysMatch(array1: Array[String], array2: Array[String]) : Boolean = {

    		  val array2Reversed = array2.reverse
    			var matches = 0

    				  for (x <- 0 until array1.length)
    				  {
    					  if (array1(x).equals(array2Reversed(x)))
    					  {
    						  matches += 1
    					  }
    				  }

    		  array1.length == matches
      }
      
      
      // Would prefer to have this a little more streamlined with a solution similar to the following, but proving tricky in Scala:
      // http://stackoverflow.com/questions/15222376/how-to-print-current-executing-junit-test-method-while-running-all-tests-in-a-cl 
      
      private def printTestMethodName()
      {
        println( "\nStarting test: " + Thread.currentThread.getStackTrace()(2).getMethodName + "\n" )
      }
      

}
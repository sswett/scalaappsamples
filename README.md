# Introduction #

This repository holds a miscellaneous collection of small, exploratory, tinkering Scala applications written by Steve Swett.

This repository is not intended for collaborative work or improvement.

# Doll Delivery Scala Programming Challenge #

Steve's solution to the [Scala Programming Challenge](https://github.com/postnati/doll-delivery) is contained within this repository.  

## Source Code ##

The source code can be found in the **doll_delivery** package.  The specific Scala function that accepts the required input and returns the required output is named **getDistanceAndPathToDestination** and is within source file **RouteCalculator.scala**.

The source code to a Scala application that calls the function (with the sample input from the challenge page) and displays the function's returned results is in file **RunDeliveryApp.scala**.

## Running the RunDeliveryApp ##

A jar file named **DollDelivery.jar** has been provided for the purpose of running the Scala application as well as the jUnit tests that were used while developing the function.

### Prepare to Run the Application and the jUnit Tests ###

The Scala application was developed with Scala version 2.11.5.

* Browse this repository's source.  Under the **jars** directory, locate **DollDelivery.jar** and download it to a directory on your computer.
* Make sure the Scala programming environment has been installed on your computer.  (Verify that **scala -version** runs successfully.)
 

### To Run the Application ###

* Using a command line, go to the directory containing **DollDelivery.jar**
* Issue the following command: 

**scala -cp DollDelivery.jar doll_delivery.RunDeliveryApp**

### To Run the jUnit Tests ###

* Using a command line, go to the directory containing **DollDelivery.jar**
* Issue the following command: 

**scala -cp DollDelivery.jar org.junit.runner.JUnitCore tests.DollDeliveryTest**